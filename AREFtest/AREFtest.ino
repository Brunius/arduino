void setup() {
  // put your setup code here, to run once:
  pinMode(3, OUTPUT);
  Serial.begin(115200);
  Serial.println("Setup complete");
}

void loop() {
  // put your main code here, to run repeatedly:
  for (int i = 0; i < 256; i++) {
    analogWrite(3, i);
    Serial.println(i);
    delay(200);
  }
  delay(2000);
}
