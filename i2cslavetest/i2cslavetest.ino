#include <Wire.h>

void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  Wire.begin(0x12);
  Wire.onReceive(handler);
  Serial.begin(115200);
  Serial.println("Setup Complete");
}

void loop() {
  // put your main code here, to run repeatedly:
}

bool state = false;

void handler(int discard) {
  Serial.println("Gotem");
  while (Wire.available() > 1) {
    Serial.write(Wire.read());
  }
  if (Wire.available()) {
    Serial.write(Wire.read());
    Serial.println();
  }
  if (state) {
    digitalWrite(13, HIGH);
    digitalWrite(12, LOW);
    digitalWrite(11, HIGH);
    digitalWrite(10, LOW);
    state = false;
  } else {
    digitalWrite(13, LOW);
    digitalWrite(12, HIGH);
    digitalWrite(11, LOW);
    digitalWrite(10, HIGH);
    state = true;
  }
}

