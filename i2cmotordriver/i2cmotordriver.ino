#include <Wire.h>
#include <EEPROM.h>

#define mot1DIR 2
#define mot1PWM 3
#define mot2DIR 4
#define mot2PWM 5
#define mot3DIR 7
#define mot3PWM 6
#define mot4DIR 8
#define mot4PWM 9

void setup() {
  // put your setup code here, to run once:
  pinMode(mot1DIR, OUTPUT);
  pinMode(mot1PWM, OUTPUT);
  pinMode(mot2DIR, OUTPUT);
  pinMode(mot2PWM, OUTPUT);
  pinMode(mot3DIR, OUTPUT);
  pinMode(mot3PWM, OUTPUT);
  pinMode(mot4DIR, OUTPUT);
  pinMode(mot4PWM, OUTPUT);
  
  int address = EEPROM.read(0x00);
  Wire.begin(address);
  Wire.onReceive(receive);
  Wire.onRequest(request);
  Serial.begin(115200);
  Serial.print("Setup Complete - address ");
  Serial.println(address);
}

void loop() {
  // put your main code here, to run repeatedly:
}

void receive(int numBytes) {
  Serial.write(Wire.read());
  //First two bits - motor address
  //Second two bits - forward/backward/brake/coast
}

void request() {
  //Send firmware revision, date, fault conditions, and address.
  //Format should be 'Pi Zero I2C HAT - firmware rev1, 2016-07-19\n(optional)FAULT: OUTPUT 1 + 2 OFFLINE\n(optional)FAULT: OUTPUT 3 + 4 OFFLINE\nHat Address: 0x12\n'
}

