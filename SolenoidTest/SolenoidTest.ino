void setup() {
  // put your setup code here, to run once:
  pinMode(10, OUTPUT);
  pinMode(13, OUTPUT);
  Serial.begin(115200);
  Serial.println("Setup complete");
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13, LOW);
  digitalWrite(10, HIGH);
  Serial.println("PWM HIGH, DIR LOW");
  delay(2000);
  digitalWrite(13, HIGH);
  digitalWrite(10, HIGH);
  Serial.println("PWM HIGH, DIR HIGH");
  delay(2000);
  for (int i = 0; i < 256; i++) {
    Serial.print("PWM ");
    Serial.print(i);
    Serial.println(", DIR LOW");
    digitalWrite(13, LOW);
    analogWrite(10, i);
    delay(200);
  }
}
